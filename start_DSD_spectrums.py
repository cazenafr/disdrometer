#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 14 10:34:57 2022

@author: Fred Cazenave
 Info to re-install package pypsd under a ipython console : 
     Change directory to PyDSD-master
     !python setup.py install
then 
 cp -r /Users/fred/opt/anaconda3/lib/python3.9/site-packages/PyDSD-0+unknown-py3.9.egg /Users/fred/opt/anaconda3/envs/top/lib/python3.9/site-packages
"""

import sys

#sys.path.append("/Users/fred/opt/anaconda3/envs/top/lib/python3.9/site-packages")

# Only to change spyder graphic environement 
#for p in sys.path:
#    print (p)
# to plot under qt5 
#%matplotlib qt5
# to plot inline in the spyder window 
#%matplotlib inline

from os import remove, listdir, chdir
from os.path  import basename, dirname, splitext, exists, getsize, abspath
import matplotlib.pyplot as plt
import numpy as np
import pytmatrix
import datetime as dt
import pandas as pd

from sklearn.metrics import r2_score
from scipy import stats

#sys.path.append("/Users/fred/opt/anaconda3/envs/top/lib/python3.9/site-packages/PyDSD-0+unknown-py3.9.egg")
import pydsd
#from  pydsd.utility.print_utility import  print_dsd_parameters
#from  pydsd.utility.func_utils import get_events, get_rain_accumulation, conv_strat_bringi_09
from  pydsd.utility.filter import filter_spectrum_with_parsivel_matrix, filter_nd_on_dropsize

chdir("/Users/fred/Documents/Codes/Python/PyDSD-master/pydsd/tests/")

#import dash
#import dash_core_components as dcc
#import dash_html_components as html

#sys.path.append("/Users/fred/opt/anaconda3/envs/top/lib/python3.9/site-packages")

try :
    exec(open(dirname(__file__)+'/../utility/func_utils.py').read())
except:
    exec(open('../utility/func_utils.py').read())
try :
    exec(open(dirname(__file__)+'/../utility/print_utility.py').read())
except:
    exec(open('../utility/print_utility.py').read())
try :
    exec(open(dirname(__file__)+'/../io/NetCDFWriter.py').read())
except:
    exec(open('../io/NetCDFWriter.py').read())
try :
    exec(open(dirname(__file__)+'/../plot/plot.py').read())
except:
    exec(open('../plot/plot.py').read())
try :
    exec(open(dirname(__file__)+'/../plot/other_plots.py').read())
except:
    exec(open('../plot/other_plots.py').read())
try :
    exec(open(dirname(__file__)+'/../../../Utils/ftp_update_file.py').read())
except:
    exec(open('../../../Utils/ftp_update_file.py').read())
try :
    exec(open(dirname(__file__)+'/../utility/dsd_TS_image_web.py').read())
except:
    exec(open('../utility/dsd_TS_image_web.py').read())
try :
    exec(open(dirname(__file__)+'/../io/ParsivelReader_Campbell.py').read())
except:
    exec(open('../io/ParsivelReader_Campbell.py').read())
    

'''
Nash-Sutcliffe Efficiency (NSE) calculation
'''
def nse(predictions, targets):
    return (1-(np.sum((predictions-targets)**2)/np.sum((targets-np.mean(targets))**2)))

'''
   This variables have to be set depending on user computer 
'''   
path_out_nc ='/Users/fred/Data/Parsivel/NCDF_OUT/'
dir_plots = '/Users/fred/Documents/Recherche/DSD/Plots/'

'''
   This variables have to be set to select Parsivel files 
''' 
Parsi_filename='/Users/fred/Data/Parsivel/OHMCV_fr_Chamrousse_Disdro_Parsivel_2.dat'
Spect_filename='/Users/fred/Data/Parsivel/OHMCV_fr_Chamrousse_Disdro_Parsivel_Spectre_2.dat'
#Parsi_filename="/Users/fred/Data/Parsivel/2021-MTO_OSUG-B_Parsivel.dat"
#Spect_filename="/Users/fred/Data/Parsivel/2021-MTO_OSUG-B_Spectre.dat"
#Parsi_filename='/Users/fred/Data/Parsivel/Campus_fr_OsugB_Mto_Parsivel2_2022-01-06.dat'
#Spect_filename='/Users/fred/Data/Parsivel/Campus_fr_OsugB_Mto_Parsivel2_Spectre.dat'
Parsi_filename='/Users/fred/Data/Parsivel/Campus_fr_OsugB_Mto_Parsivel2.dat'
Spect_filename='/Users/fred/Data/Parsivel/Campus_fr_OsugB_Mto_Parsivel2_Spectre.dat'
#Parsi_filename='/Users/fred/Data/Parsivel/test/Campus_fr_OsugB_Mto_Parsivel2_2022-01-06.dat'
#Spect_filename='/Users/fred/Data/Parsivel/test/Campus_fr_OsugB_Mto_Parsivel2_Spectre.dat'
Parsi_filename='/Users/fred/Data/Parsivel/CARE/CARE_vn_SPO_Parsivel2.dat'
Spect_filename='/Users/fred/Data/Parsivel/CARE/CARE_vn_SPO_Parsivel2_Spectre.dat'

#Spect_filename='/Users/fred/Data/Parsivel/CARE/Disdro_CARE_HCMC_Parsivel2_Spectre_2022-06-03T08-59.dat'
#Parsi_filename='/Users/fred/Data/Parsivel/CARE/Disdro_CARE_HCMC_Parsivel2_2022-06-03T08-59.dat'

#Parsi_filename='/Users/fred/Data/Parsivel/OsugB_Parsivel2_20220213.dat'
#Spect_filename='/Users/fred/Data/Parsivel/OsugB_Spectre20220213.dat'

#Parsi_filename="/Users/fred/Data/Parsivel/DISDRO-OTT-PARSIVEL-CHAMROUSSE/OHMCV_fr_Chamrousse_Disdro_Parsivel_2.dat"
#Spect_filename="/Users/fred/Data/Parsivel/DISDRO-OTT-PARSIVEL-CHAMROUSSE/OHMCV_fr_Chamrousse_Disdro_Parsivel_Spectre_2.dat"

#Parsi_filename='/Users/fred/Downloads/GreEn_ER_Mto_Parsivel2-2021-semestre1.dat'
#Spect_filename='/Users/fred/Downloads/GreEn_ER_Mto_Parsivel2_Spectre-2021-semestre1.dat'

#Parsi_filename="/Users/fred/Data/Parsivel/DISDRO-OTT-PARSIVEL-ENSE3/GreEn_ER_Mto_Parsivel2.dat"
#Spect_filename="/Users/fred/Data/Parsivel/DISDRO-OTT-PARSIVEL-ENSE3/GreEn_ER_Mto_Parsivel2_Spectre.dat"



'''
   This variables have to be set to define the resampling interval 
''' 
resampling='5min'
#resampling='1H'
resampling=None
resampling='15min'
#resampling='1min'


'''
pydsd.read_parsivel_Campbell
  """
  Takes 2 filenames pointing to a parsivel raw file and a parsivel raw spectrum  
  and returns a drop size distribution object.
  start : define the starting date of sampling extraction - the nearest will be used
  if start is not set, extraction will start at the begining of the sampling 
  stop  : define the stoping date of the sampling extraction - the nearest will be used
  if stop is not set, extraction will end at the ending of the sampling 
  if start and stop are not set, all the sampling will be passed to the dsd object
  start and stop should be '%Y-%m-%d %H:%M:%S'
  
  if v_filter is set to True, the raw spectrum is multiplyed by de +or- 50% speed matrix
  
  resampling : resampling time of the dataset (in minutes)

  Usage:
  dsd = read_parsivel_Campbell(Parsi_filename,
                               Spect_filename,
                               start=None,
                               stop=None,
                               v_filter=None,
                               resampling=None)
  
   Takes 2 filenames pointing to a parsivel raw file and a parsivel raw spectrum  
   and returns a drop size distribution object.
   start : define the starting date of sampling extraction - the nearest will be used
   if start is not set, extraction will start at the begining of the sampling 
   stop  : define the stoping date of the sampling extraction - the nearest will be used
   if stop is not set, extraction will end at the ending of the sampling 
   if start and stop are not set, all the sampling will be passed to the dsd object
   start and stop should be '%Y-%m-%d %H:%M:%S'
   
   if v_filter is set to True, the raw spectrum is multiplyed by de +or- 50% speed matrix
   
   resampling : resampling time of the dataset
   the resampling format should be like \'5min\’ or \'H\' or \'D\' 
   see Offset aliases in df.resample Python documentation for more precision

   Returns:
   DropSizeDistrometer object

   in PyDSD-master/pydsd/io/ParsivelReader_Campbell.py
'''
dsd = read_parsivel_Campbell(Parsi_filename,
                                   Spect_filename,
                                    start="2022-06-01 00:00:00",
#                                    stop="2021-06-30 23:59:00",
#                                    v_filter=True,
                                    resampling=resampling
)
print("total rain=", np.sum(dsd.rain_rate["data"])/(np.nanmin(dsd.sampling_interval["data"])/60))
#print(dsd.fields.keys())




'''
Different ways to print the dates
'''
#print(dt.datetime.utcfromtimestamp(dsd.time["data"][0]).strftime('%Y-%m-%d %H:%M:%S'))
#print(dt.datetime.utcfromtimestamp(dsd.time["data"][-1]).strftime('%Y-%m-%d %H:%M:%S'))
#print(pd.to_datetime(dsd.time["data"][0], unit='s'))
#print(pd.to_datetime(dsd.time["data"][0], unit='s'))

'''
get_events(dsd,sep_event=10800,min_drops=None,min_acc=None)
    extract Start and stop event date -  sep_event is the time with no rain separating 2 events 
    #Data split in rain event could be done over
        - 2 events are plit by a dry period of time (sep_event by default is set to 3h) 
        - a minimum number drops (min_drops) could be set to considere a rainy period as a event (default None)
        - a minimum of rain acculalation (min_acc) could be set to considere a rainy period as a event (default None)
        - if min_drops is set min_acc is not used
    In PyDSD-master/pydsd/utility/func_utils.py  
''' 
#events = get_events(dsd,sep_event=10800,min_drops=1000,min_acc=0.1)
events = get_events(dsd,sep_event=10800,min_drops=None,min_acc=5)
starts=[]
for i in list(zip(*events))[0] : 
    starts.append(np.where(dsd.time["data"] == i )[0][0])

stops=[]
for i in list(zip(*events))[1] : 
    stops.append(np.where(dsd.time["data"] == i )[0][0])
    
acc=[]
for i in range(len(starts)) :
    acc.append(get_rain_accumulation (dsd, starts[i], stops[i]))

    
max_ev_start=starts[acc.index(np.nanmax(acc))]
max_ev_stop=stops[acc.index(np.nanmax(acc))]

print("Event n°",acc.index(np.nanmax(acc))," : max rain accumulation over the period : from ",
      pd.to_datetime(dsd.time["data"][max_ev_start], unit='s'),
      " to ",
      pd.to_datetime(dsd.time["data"][max_ev_stop], unit='s'),
      " : ",
      round(np.nanmax(acc),2),
      "mm")


# while True:
#     try:
#         ev = int(input("Event to traite : "))
#         if ev not in range(len(events)) : 
#             print("This is not a valid event number.")
#             continue
#     except ValueError:
#         print("This is not a valid event number.")
#         continue
#     else:
#         break

# #ev=19
# ind_start= starts[ev]
# #ev=198
# ind_stop= stops[ev]+1

# '''
# extract_subset_dsd 
#   extracts a subset of the DSD dataset over a period between start_time and stop_time
#     New dsd object for a specfic period

#     In PyDSD-master/pydsd/utility/func_utils.py 

# '''
dates=[]
D0=[]
Dm=[]
DM=[]
DC=[]
R=[]
Mu=[]

for i in range(len(events)) : 
#    if (i == 36) or (i==118) or (i==160): continue   
    new_dsd=extract_subset_dsd(dsd,range(starts[i],stops[i]+1))
    filter_spectrum_with_parsivel_matrix(new_dsd,over_fall_speed=0.5,under_fall_speed=0.5,replace=True,maintain_smallest=False)
    filter_nd_on_dropsize(new_dsd, drop_min=None, drop_max=7, replace=True)
    velocity=new_dsd.calculate_fall_speed(dsd.diameter["data"],inplace=True)
    filter_spectrum_with_parsivel_matrix(new_dsd,over_fall_speed=0.5,under_fall_speed=0.5,replace=True,maintain_smallest=False)
    new_dsd.calculate_dsd_from_spectrum(effective_sampling_area=None, replace=True)
    new_dsd.calculate_RR()
    new_dsd.calculate_dsd_parameterization()
    fig=plot_spectrums_DSD(new_dsd)
    file_name = dt.datetime.fromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d')+\
                    "_spectrums_norm"
    file_name="Spectrums/"+file_name+'.png'
    plt.savefig(dir_plots+file_name)
    plt.clf()
    fig=plot_spectrums_DSD(new_dsd,norm=False)
    file_name = dt.datetime.fromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d')+ \
                "_spectrums"
    file_name="Spectrums/"+file_name+'.png'
    plt.savefig(dir_plots+file_name)
    plt.clf()
    fig=plot_DSD_params(new_dsd)
    file_name = dt.datetime.fromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d')+ \
        "_Param_DSD"
    file_name="Spectrums/"+file_name+'.png'
    plt.savefig(dir_plots+file_name)
    plt.clf()


