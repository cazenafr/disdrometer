#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  1 15:14:17 2022

@author: fred
"""
import datetime
import numpy as np
import matplotlib as mpl
from matplotlib import dates
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.ticker as mtick
from matplotlib.ticker import MultipleLocator
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from PIL import Image
from pylab import cm
from matplotlib.dates import DateFormatter

def generate_TS_analyse_img(dsd,
                            xlims=None,
                            ylims=None,
                            log_scale=True,
                            dmin=None,
                            dmax=None,
                            cmap=None,
                            pos_leg=None,
                            add_title=None,
                            plot=True,
                            save=True) : 
    
    dir_plots = '/Users/fred/Documents/Recherche/DSD/Plots/'

    Nlayer = 4
    
    fig, ax = plt.subplots(Nlayer, 1, sharex=True)
    
    lastfor= dsd.time["data"][-1]-dsd.time["data"][0] 
    
    if lastfor <= 2880 : 
        hh_mm = DateFormatter('%H:%M')
    else : 
        hh_mm = DateFormatter('%d-%m')
        
    ax[0].xaxis.set_major_formatter(hh_mm)
    
   
    
    sampling = int(dsd.sampling_interval["data"].min())
    rain_rate=dsd.rain_rate["data"]  
    rain_rate=np.nan_to_num(rain_rate, copy=True, nan=0.0)
    x=[datetime.datetime.fromtimestamp(d) for d in dsd.time["data"]]
    c = ax[0].step(x,rain_rate,color='b')
    ax[0].set_ylabel("Rain rate (mm/h)",color='b')
    ml = MultipleLocator(5)
    ax[0].yaxis.set_minor_locator(ml)
    ax0_twinx = ax[0].twinx()
    ax0_twinx.plot(x,np.multiply(rain_rate.cumsum(),sampling/3600),"r-")
    ax0_twinx.set_ylabel(r'$\sum(R)$ (mm)',color='r')
    ml = MultipleLocator(5)
    ax0_twinx.yaxis.set_minor_locator(ml)
    
    ax[0].set(title='HCMC Rainfall event from '+str(x[0])+' to '+str(x[-1])+' (UTC)')
    
    norm=None
    vmin=0
    vmax = np.nanmax(dsd.fields["Nd"]["data"])
    if log_scale is True : 
        vmin = 0.1
        norm = mpl.colors.LogNorm(vmin=vmin, vmax=vmax)
    else : log_scale=None
        
    if cmap is None : 
        #cmap = plt.cm.get_cmap("viridis_r").copy()
        #cmap.colors[0]=[1,1,1]
        colors = [("white")] + [(cm.jet(i)) for i in range(1, 256)]
        cmap = mpl.colors.LinearSegmentedColormap.from_list("new_map", colors, N=256)
        
    if dmin is None:
        dmin = np.nanmin(dsd.diameter["data"])
       
    if dmax is None:
        dmax = np.nanmax(dsd.diameter["data"])
        
    diam = dsd.diameter["data"][np.where((dsd.diameter["data"] >= dmin) & 
                                             (dsd.diameter["data"] <= dmax))[0]]
    
    Nd=dsd.fields["Nd"]["data"][:,np.where((dsd.diameter["data"] >= dmin) & 
                                             (dsd.diameter["data"] <= dmax))[0]]
      
    c = ax[1].pcolor(x,diam,Nd.T,
                     linewidths=0,cmap=cmap,norm=norm)
  
    ax[1].set_xlabel("Time(m)")
    ax[1].set_ylabel("Diameter(mm)")
    ml = MultipleLocator(1)
    ax[1].yaxis.set_minor_locator(ml)
    # Add the colorbar inside...
    colbarlabel='Drops (m$^{-3}$ mm$^{-1}$)' 
    cbaxes = inset_axes(ax[1], width="50%", height="5%", loc='upper right') 
    plt.colorbar(c, cax=cbaxes,label=colbarlabel,orientation='horizontal')  
    
    ax[1].plot(x,dsd.fields["D0"]["data"],'r-',label='D0')
    ax[1].plot(x,dsd.fields["Dm"]["data"],'b-',label='Dm')
    if pos_leg == None :
        ax[1].legend(bbox_to_anchor=(1.02, 1), loc='upper left', borderaxespad=0)
    else : 
        ax[1].legend(loc=pos_leg)
    
    ax[2].plot(x,dsd.fields["Nw"]["data"],"g-")
    ax[2].set_ylabel("Nw (m$^{-3}$ mm$^{-1}$)")
    ax[2].yaxis.get_major_formatter().set_useOffset(False)
#    ax[2].ticklabel_format(axis='y', style='sci')
#    ax[2].yaxis.set_major_formatter(mtick.FuncFormatter(lambda value,pos: ("$10^{%d}$" % pos) ))
#    ax[2].yaxis.set_major_formatter(mtick.ScalarFormatter(useMathText=True))
#   ml = MultipleLocator(1000)
#   ax[2].yaxis.set_minor_locator(ml)
    
    ax[3].plot(x,dsd.fields["Zh"]["data"],"b-")
    ax[3].set_ylabel("Zh (dBZ)")
    ml = MultipleLocator(10)    
    ax[3].yaxis.set_major_locator(ml)
    ml = MultipleLocator(5)
    ax[3].yaxis.set_minor_locator(ml)
    fig.set_size_inches(11.69,8.27)
      
#    plt.show()
    if save :
        if add_title == None :
            file_name = 'DSD_TS_HCMC_'+str(x[0])+'_'+str(x[-1])+'_'+str(sampling)+'s'
        else :
            file_name = 'DSD_'+str(add_title)+'_TS_HCMC_'+str(x[0])+'_'+str(x[-1])+'_'+str(sampling)+'s'
        file_name=file_name.replace(' ','_')    
        print(file_name)
        fig.savefig(dir_plots+file_name+'.png')
        plt.clf()
        plt.close()
        
    return fig





