#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 15 11:02:10 2022

@author: fred
"""
import datetime
import numpy as np
import decimal
import math
from math import log10,gamma
import matplotlib
from matplotlib import dates
from matplotlib import gridspec
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib.dates import DateFormatter
import pandas as pd

#from  pydsd.utility.func_utils import extract_subset_dsd, conv_strat_bringi_09

def plot_dsd_characteristics(dsd,var):
    '''
        Parameters
        ----------
        dsd : TYPE
            DESCRIPTION.
        var : TYPE np.array 
            DESCRIPTION.
                give all the dsd fields to display
                ex : ["Nd","rain_rate","D0","Nw"]
        Returns
        -------
        None.
    '''
    x = [datetime.datetime.utcfromtimestamp(dsd.time["data"][i].astype(float)) for i in range(len(dsd.time["data"]))]
    # Plot seperate axes
    if len(var) > 1 :
        fig, axes = plt.subplots(nrows=len(var), sharex=True)
    else :
        axes=[0]
        fig, axes[0]= plt.subplots()
    axes[0].set_title("From "+x[0].strftime('%Y-%m-%d %H:%M:%S')+" to "+x[-1].strftime('%Y-%m-%d %H:%M:%S'),
               loc="center")
    i=0
    pal = plt.cm.get_cmap("viridis_r").copy()
    pal.colors[0]=[1,1,1]
    for j in var :
        if j == 'rain_rate' :
            axes[i].step(x,dsd.fields["rain_rate"]["data"],color='b')
            axes[i].set_ylabel('Rain rate (mm/h)',color='r',fontsize=6)
            ax2 = axes[i].twinx()
            ax2.plot(x,np.multiply(dsd.fields["rain_rate"]["data"].cumsum(),dsd.sampling_interval['data'][0]/3600),"r-")
            ax2.set_ylabel('Rain accumulation\n(mm)',color='r',fontsize=6)
        if j in ("D0","Dm") : 
            axes[i].plot(x,dsd.fields[j]["data"],color='b')
            axes[i].set_ylabel(r" diameter (mm)",color='b',fontsize=6)
        if j in ("N0","Nw"):
                axes[i].plot(x,np.log10(dsd.fields[j]["data"]),color='b')
                axes[i].set_ylabel(r" Drop number",color='b',fontsize=6)
        if j ==  "Nd" : 
           im=axes[i].pcolormesh(
              x,
              dsd.diameter["data"],
              dsd.fields["Nd"]["data"].T,
              vmin=np.nanmin(dsd.fields["Nd"]["data"]),
              vmax=np.nanmax(dsd.fields["Nd"]["data"]),
              cmap=pal,
              )
           axes[i].set_ylim(0., dsd.diameter["data"][-1])
           axes[i].set_ylim(0, 3)
           fig.subplots_adjust(left=0.07, right=0.87)
           # Add the colorbar outside...
           box = axes[i].get_position()
           pad, width = 0.02, 0.02
           cax = fig.add_axes([box.xmax + pad, box.ymin, width, box.height])
           fig.colorbar(im, cax=cax)
        axes[i].tick_params(axis='both', labelsize=6)
        i+=1
    ax = plt.gcf().axes[i-1] 
    ax = tsindex(ax)
    plt.gcf().autofmt_xdate(rotation=35)


def plot_all_Nd(dsd,cols=4) :
    np.seterr(divide = 'ignore') 
    xmax=log10(dsd.fields["Nd"]["data"].max())
    figs=dsd.fields["Nd"]["data"].shape[0]
    rows = math.ceil(figs / cols)
    fg, ax = plt.subplots(rows, cols)
    for i in range(rows) : 
        for j in range(cols) : 
            if i*cols+j < figs :
                ax[i, j].plot(dsd.diameter["data"], np.log10(dsd.fields["Nd"]["data"][i*cols+j]))
                title=datetime.datetime.utcfromtimestamp(dsd.time["data"][i*cols+j]).strftime('%Y/%m/%d %H:%M:%S')                                                            
                if "Nt" in list(dsd.fields.keys()) :
                    title += '\nNt ='+str(round(dsd.fields["Nt"]["data"][i*cols+j],2))
                    title += ' D0 ='+str(round(dsd.fields["D0"]["data"][i*cols+j],2))
                    title += ' Dm ='+str(round(dsd.fields["Dm"]["data"][i*cols+j],2))
#                    ax[i, j].text(0.5,2.5,text,fontsize=8)   
                ax[i, j].set_title(title,fontsize = 8,x=0.55, y=0.45)
                ax[i, j].set_xlim([dsd.diameter["data"][2], 5])
                ax[i, j].set_ylim([0, xmax])
                ax[i, j].yaxis.label.set_size(7)
                if j == 0 and i == round(rows/2) : 
                    ax[i, j].set_ylabel(r'$\log10(m^{-3} mm^{-1})$')
            ax[i, j].tick_params(axis='both', labelsize=6)
    fg.set_size_inches(11.69,8.27)
    np.seterr(divide = 'warn') 
    return fg 

def plot_drop_spectrum(
        dsd, 
        xlabel="",
        ylabel="", 
        colbarlabel="", 
        title="", 
        xlim=None, 
        ylim=None, ):
    pal = copy.copy(plt.cm.get_cmap("viridis_r"))
    pal.colors[0]=[1,1,1]
    fig, ax = plt.subplots()
    
    drop_spectrum=np.nan_to_num(dsd.fields["drop_spectrum"]["data"][:,:,:], copy=True, posinf=0)
    
    im=ax.pcolormesh(np.insert(dsd.diameter["data"],0,0),
                      np.insert(dsd.spectrum_fall_velocity["data"],0,0),
                      np.log10(drop_spectrum.sum(axis=0)),
                      shading='flat', cmap=pal)
    if xlim != None :
        ax.set_xlim(xlim)
    if ylim != None :
        ax.set_ylim(ylim)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.colorbar(im,label=colbarlabel)    
    return fig, ax

def plot_Nw_vs_Dm(
        dsd, 
        xlabel="",
        ylabel="", 
        colbarlabel="", 
        title="", 
        xlim=None, 
        ylim=None, ):
    
    pal = plt.cm.get_cmap("viridis_r").copy()
    pal.colors[0]=[1,1,1]
    fig, ax = plt.subplots()
    test=dsd.fields["Nt"]["data"]/dsd.fields["Nt"]["data"].sum()*100
    for i in range(dsd.fields["Nt"]["data"].shape[0]-1) : test =  np.column_stack ((test,dsd.fields["Nt"]["data"]/dsd.fields["Nt"]["data"].sum()*100))
    im=ax.pcolormesh(dsd.fields["Dm"]["data"],
                      np.log10(dsd.fields["Nw"]["data"]),
                      test,
                      shading='flat', cmap=pal)
    if xlim != None :
        ax.set_xlim(xlim)
    if ylim != None :
        ax.set_ylim(ylim)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.colorbar(im,label=colbarlabel)        
    return fig, ax


def hyetogram (dsd,ax=None,fig=None,start=None) :
    #   Parameter start set a starting time before the first spectra
    #   Parameter start should be specified in seconds
    
    sampling = int(dsd.sampling_interval["data"].min())
#    x= pd.to_datetime(dsd.time["data"], unit='s',utc=True) 
    rain_rate=dsd.fields["rain_rate"]["data"]  
    x=[datetime.datetime.utcfromtimestamp(d) for d in dsd.time["data"]]
#    ax = parse_ax(ax)
#    fig = parse_fig(fig)
    ax=plt.gca()
    plt.step(x,rain_rate,color='b')
    if start != None : 
        ax.set_xlim(x[0] - datetime.datetime.timedelta(seconds=start),x[-1])
    plt.ylabel("Rain rate (mm/h)",color='b')
    ax2 = plt.twinx()
    ax2.plot(x,np.multiply(rain_rate.cumsum(),sampling/3600),"r-")
    ax2.set_ylabel(r"Rain accumulation (mm)",color='r')
    plt.gcf().autofmt_xdate()
#    plt.title("From "+x[0].strftime('%Y-%m-%d %H:%M:%S')+" to "+x[-1].strftime('%Y-%m-%d %H:%M:%S'))
    plt.title("From "+str(x[0])+" to "+str(x[len(x)-1]))
    ax = tsindex(ax)
    return ax

def dsd_contrib2rain(dsd,ind=0):
    tmp=np.ma.zeros((dsd.numt,len(dsd.spread["data"])))
    for t in range(0, dsd.numt-1):
        tmp[t]= 0.6 / 12 * np.pi * 1e-03 * dsd._mmultiply(
            dsd.velocity["data"],
            dsd.Nd["data"][t],
            dsd.spread["data"],
            np.array(dsd.diameter["data"]) ** 3,
        )
    plt.xlim(0,10)
    plt.bar(dsd.diameter["data"],tmp[ind])
    plt.xlabel("Diameters",color='w')
    plt.ylabel("Rain acc. per diam. class (mm)",color='b')
    ax2 = plt.twinx()
    ax2.plot(dsd.diameter["data"],np.cumsum(tmp[ind]),color='r')
    ax2.set_ylim(0,np.ceil(np.sum(tmp[ind])))
    ax2.set_ylabel(r"Rain accumulation (mm)",color='r')
    title= datetime.datetime.utcfromtimestamp(dsd.time["data"][ind]).strftime('%Y-%m-%d %H:%M:%S')
    plt.title(title+" ("+str(dsd.sampling_interval["data"][ind])+"s)")
    x=(np.where(np.sum(tmp[ind])/2 >= np.cumsum(tmp[ind])))[0][-1]
    plt.scatter(x,np.sum(tmp[ind]/2), marker="*")

def plot_conv_strat_BR09(dsd) :
    
    # Use cross product to determine whether a point lies above or below a line.
    #   Math: https://math.stackexchange.com/a/274728
    #   English: "above" means that looking from point a towards point b, 
    #               the point p lies to the left of the line.
    is_above = lambda p,a,b: np.cross(p-a, b-a) < 0
    
        
    # Bringi et al 2009 have define a separation line between Convective and Stratiforme
    # sample of DSD on ascatter plot D0, log10(Nw)
    
    fig = plt.figure()
    
    # to change size of subplot's
    # set height of each subplot as 8
    fig.set_figheight(4)
    fig.set_figwidth(10)
    
    # create grid for different subplots
    spec = gridspec.GridSpec(ncols=2, nrows=1,
                             width_ratios=[1, 4], wspace=0.3,
                             hspace=0.05)
     
    
    BR09_a=-1.6
    BR09_b=6.3   
    
    xlab = r"D$_0$ (mm)"
    ylab = r"log$_{10}$[N$_w$] (mm$^{-1}$ m$^{-3}$)"
    
    cols=[datetime.datetime.utcfromtimestamp(d) for d in dsd.time["data"]]
    cols=pd.to_datetime(cols,utc=True).time
    
    colors = np.array([[255,0,0], [0,255,0]])       
    cols=[colors[1] if  dsd.time["data"][i] > 1654137000 else colors[0] for i in range(dsd.numt)]
    cmap= matplotlib.colors.ListedColormap(colors)
    
    #fig, ax0 = plt.subplots()
    y=np.log10(dsd.fields["Nw"]["data"])
    x=dsd.fields["D0"]["data"]
    cmap.set_under(color='black')  
    ax0 = fig.add_subplot(spec[0])  
    ax0.scatter(x, y, s=20)  
    #   plt.scatter(x, y, s=20, c=np.array(cols)/255., cmap=cmap)
    xx=np.arange(np.nanmin(x),np.nanmax(x),0.1)
    ax0.plot(xx,BR09_a*xx+BR09_b,'r-.',label='Sep. conv/strat')
    ax0.set_ylim([1,5])
        #plt.plot(x,x*0+3.85,'r-')   
    #    cb=plt.colorbar(ticks=[dsd.time["data"][0], dsd.time["data"][dsd.numt//2], dsd.time["data"][-1]])
    #   cb.ax.set_yticklabels([cols[0], cols[dsd.numt//2], cols[-1]])
    ax0.set_xlabel(xlab)
    ax0.set_ylabel(ylab)
    ax0.legend(loc='lower left')
    
    ax1 = fig.add_subplot(spec[1])  
    
    dt=[datetime.datetime.utcfromtimestamp(d) for d in dsd.time["data"]]
    plt.plot(dt,dsd.fields["rain_rate"]["data"])
    a= np.array([1,BR09_a+BR09_b])
    b= np.array([3,BR09_a*3+BR09_b])
#   conv_strat=[is_above(np.array(x[i],y[i]),a,b) for i in range(dsd.numt)]
    for i in range(dsd.numt) :
        if is_above([x[i],y[i]],a,b):
            ax1.scatter(dt[i], dsd.fields["rain_rate"]["data"][i], color='red',s=10)
        else:
            ax1.scatter(dt[i], dsd.fields["rain_rate"]["data"][i], color='green',s=10)   
    ax1 = tsindex(ax1)
    ax1.set_xlabel("time")
    ax1.set_ylabel("Intensity (mm/h)")
    ax1.set_title(dt[0].isoformat(' ')+" rainfall event, automatic classification conv/strat")
    
    return fig


def plot_conv_strat_DSD(dsd,plt_gamma=False, norm = True) :
    
    '''
    To get only the convective part of the rain 

    ''' 
    conv_dsd=extract_subset_dsd(dsd,np.where(conv_strat_bringi_09 (dsd)))
    strat_dsd=extract_subset_dsd(dsd,np.where(np.invert(conv_strat_bringi_09 (dsd))))
#    strat_dsd_ocean=extract_subset_dsd(dsd,np.where(np.invert(conv_strat_Thompson_15 (dsd))))
    
    conv_dsd.fields["Nd"]["data"][np.where(conv_dsd.fields["Nd"]["data"]==0)]=np.nan
    strat_dsd.fields["Nd"]["data"][np.where(strat_dsd.fields["Nd"]["data"]==0)]=np.nan
   
    Nlayers = 1
    Ncols = 2
    fig, ax = plt.subplots(Nlayers, Ncols, sharex=False)
    ax[0].plot(conv_dsd.diameter["data"], conv_dsd.fields["Nd"]["data"][0],'.',color='silver',label='DSD')  
    for i in range(1,len(conv_dsd.fields["rain_rate"]["data"])) :
          ax[0].plot(conv_dsd.diameter["data"][2:-1], conv_dsd.fields["Nd"]["data"][i][2:-1],'.',color='silver') 
    ax[0].plot(conv_dsd.diameter["data"][2:-1], np.nanmean(conv_dsd.fields["Nd"]["data"],axis=0)[2:-1],color='black',label='Mean DSD')
    if plt_gamma == True :
        if norm == True : 
            psd=get_GammaDSD (conv_dsd) 
        else :
            psd=get_UnnormalizedGammaDSD (conv_dsd) 
        ax[0].plot(conv_dsd.diameter["data"][2:-1],psd[2:-1],color='red')
    ax[0].set_xlim(0,10)
    ax[0].set_ylim(0.1,np.nanmax(conv_dsd.fields["Nd"]["data"]))
    ax[0].set_xlabel("Diameters (mm)")
    ax[0].set_yscale('log')
    ax[0].set_ylabel(r'$N(d)(m^{-3} mm^{-1})$')
    ax[0].set_title("Convective rainfall DSD")
    ax[0].legend(loc='upper right')


    ax[1].plot(strat_dsd.diameter["data"], strat_dsd.fields["Nd"]["data"][0],'.',color='silver',label='DSD')  
    for i in range(1,len(strat_dsd.fields["rain_rate"]["data"])) :
          ax[1].plot(strat_dsd.diameter["data"][2:-1], strat_dsd.fields["Nd"]["data"][i][2:-1],'.',color='silver') 
    ax[1].plot(strat_dsd.diameter["data"][2:-1], np.nanmean(strat_dsd.fields["Nd"]["data"],axis=0)[2:-1],color='black',label='Mean DSD') 
    if plt_gamma == True :
        if norm == True : 
            psd=get_GammaDSD(strat_dsd) 
        else:
            psd=get_UnnormalizedGammaDSD (strat_dsd) 
        ax[1].plot(strat_dsd.diameter["data"][2:-1],psd[2:-1],color='red')
    ax[1].set_xlim(0,10)
    ax[1].set_ylim(0.1,np.nanmax(conv_dsd.fields["Nd"]["data"]))
    ax[1].set_xlabel("Diameters (mm)")
    ax[1].set_yscale('log')
    ax[1].set_ylabel(r'$N(d)(m^{-3} mm^{-1})$')
    ax[1].set_title("Stratiform rainfall DSD")
    ax[1].legend(loc='upper right')
    return fig

def plot_spectrums_DSD(dsd,norm = True,) : 
   Nlayers = 1
   Ncols = 1
   colors = plt.cm.rainbow(np.linspace(1, 0, len(dsd.fields["Nd"]["data"])))
   DC = calculate_Dc(dsd)
   DM = dsd._calc_mth_moment(1)/dsd.fields['Nt']['data']
   Nd=copy.deepcopy(dsd.fields["Nd"]["data"])
   Nd[np.where(Nd == 0)] = np.nan
   fig, ax = plt.subplots(Nlayers, Ncols, sharex=False)
# DM= Dmean, Dm = D mean, D0 = D median and Dc = D Characteristic M4/M3 
   data=f"{'date':<9}{'DM(mm)':>10}{'Dm(mm)':>8}{'DC(mmm)':>9}{'Mu':>5}{'Nt':>11}{'R(mm/h)':>12}"
   fig.text(0.6,0.98,data,ha='left', va='top', size=8,color='k')

   for i in range(len(dsd.fields["Nd"]["data"])):
        if np.sum(dsd.fields["Nd"]["data"][i]) > 0 : 
        #label = 
            if norm : 
                data=f"{dt.datetime.utcfromtimestamp(dsd.time['data'][i]).strftime('%H:%M:%S'):<9}"+\
                    f"{np.round(DM[i],2):>9}"+\
                    f"{np.round(dsd.fields['D0']['data'][i],2):>11}"+\
                    f"{np.round(dsd.fields['Dm']['data'][i],2):>12}"+\
                    f"{np.round(dsd.fields['mu']['data'][i],2):>14}"+\
                    f"{np.round(dsd.fields['Nt']['data'][i],0):>10}"+\
                    f"{np.round(dsd.fields['rain_rate']['data'][i],0):>10}"
                fig.text(0.60,0.98-0.025*(i+1),data,ha='left', va='top', size=8,color=colors[i])
                ax.plot(dsd.diameter["data"]/dsd.fields["D0"]["data"][i], Nd[i]*dsd.fields["D0"]["data"][i]/np.sum(dsd.fields["Nd"]["data"][i]),'-',color=colors[i])  
                #print(dt.datetime.utcfromtimestamp(dsd.time["data"][i]).strftime('%Y-%m-%d %H:%M:%S'),np.round(dsd.fields["D0"]["data"][i],2),np.round(dsd.fields["Dm"]["data"][i],2),np.round(dsd.fields["mu"]["data"][i],2),np.sum(dsd.fields["Nd"]["data"][i]))
                ax.set_xlim(0,4)
             #   ax.set_ylim(0.1,np.nanmax(dsd.fields["Nd"]["data"]))
                ax.set_xlabel("D/Dc")
                ax.set_yscale('log')
                ax.set_ylabel(r'N(d)*Dc/Nt')
            else : 
                data=f"{dt.datetime.utcfromtimestamp(dsd.time['data'][i]).strftime('%H:%M:%S'):<9}"+\
                    f"{np.round(DM[i],2):>9}"+\
                    f"{np.round(dsd.fields['D0']['data'][i],2):>11}"+\
                    f"{np.round(dsd.fields['Dm']['data'][i],2):>12}"+\
                    f"{np.round(dsd.fields['mu']['data'][i],2):>14}"+\
                    f"{np.round(dsd.fields['Nt']['data'][i],0):>10}"+\
                    f"{np.round(dsd.fields['rain_rate']['data'][i],0):>10}"
                fig.text(0.60,0.98-0.025*(i+1),data,ha='left', va='top', size=8,color=colors[i])
                ax.plot(dsd.diameter["data"], Nd[i],'-',color=colors[i])  
                ax.set_xlim(0,10)
             #   ax.set_ylim(0.1,np.nanmax(dsd.fields["Nd"]["data"]))
                ax.set_xlabel("Diameters (mm)")
                ax.set_yscale('log')
                ax.set_ylabel(r'$N(d)(m^{-3} mm^{-1})$')  
   
   ax.set_title("rainfall DSD "+dt.datetime.utcfromtimestamp(dsd.time["data"][0]).strftime('%Y-%m-%d'))
   fig.set_size_inches(11.69,8.27)
   return fig

def plot_DSD_params(dsd) : 
   Nlayers = 2
   Ncols = 1
   DC = calculate_Dc(dsd)
   DM = dsd._calc_mth_moment(1)/dsd.fields['Nt']['data']
   Nd=copy.deepcopy(dsd.fields["Nd"]["data"])
   Nd[np.where(Nd == 0)] = np.nan
   # DM= Dmean, Dm = D mean, D0 = D median and Dc = D Characteristic M4/M3 
   
   fig, ax = plt.subplots(Nlayers, Ncols, sharex=True)
   date_str=[dt.datetime.utcfromtimestamp(dsd.time['data'][i]).strftime('%H:%M:%S') for i in range(len(dsd.fields["Nd"]["data"]))]
   ln1=ax[0].plot(date_str,dsd.fields['D0']['data'],'b',label='D0')
#   ax[0].plot(date_str,dsd.fields['Dm']['data'],'r',label='Dm')
   ln2=ax[0].plot(date_str,DM,'g',label='DM')
   ln3=ax[0].plot(date_str,DC,'k',label='DC')
   ax[0].set_ylabel("Diameters (mm)")
   ax2 = ax[0].twinx()
   ln4=ax2.plot(date_str,dsd.fields['mu']['data'],'r--',label='Mu')
   ax2.set_ylabel("Mu")   
   # added these three lines
   lns = ln1+ln2+ln3+ln4
   labs = [l.get_label() for l in lns]
   ax[0].legend(lns, labs, loc='upper right')

   ln1=ax[1].plot(date_str,dsd.fields['rain_rate']['data'],'b',label='Rain_rate')  
   ax[1].set_ylabel("Rain_rate (mm/h)")
   ax2 = ax[1].twinx()
   ln2=ax2.plot(date_str,dsd.fields['Nt']['data'],'r',label='Nt')
   ax2.set_ylabel("Nt") 
   ax[1].tick_params(axis='x', labelrotation = 45)
   # added these three lines
   lns = ln1+ln2
   labs = [l.get_label() for l in lns]
   ax[1].legend(lns, labs, loc='upper right')
   fig.set_size_inches(11.69,8.27)
   return fig
    
def tsindex(ax):
    """
        Reset the axis parameters to look nice!
        From Project: py-openaq
    """
    # Get dt in days
    dt = ax.get_xlim()[-1] - ax.get_xlim()[0]

#    if dt <= 1./24.:   # less than one hour
#       pass
#   elif dt <= 2.:     # less than 2 days
    if dt <= 2 : 
        hh_mm = DateFormatter('%H:%M')
        ax.xaxis.set_major_formatter(hh_mm)
#        ax.xaxis.set_minor_locator( dates.HourLocator() )
#       ax.xaxis.set_minor_formatter( dates.DateFormatter(""))

#        ax.xaxis.set_major_locator( dates.HourLocator( interval=3))
#        ax.xaxis.set_major_formatter( dates.DateFormatter("%-I %p"))
    elif dt <= 7.:      # less than one week
        ax.xaxis.set_minor_locator( dates.DayLocator())
        ax.xaxis.set_minor_formatter( dates.DateFormatter("%d"))

        ax.xaxis.set_major_locator( dates.DayLocator( bymonthday=[1, 8, 15, 22]) )
        ax.xaxis.set_major_formatter( dates.DateFormatter("\n%b\n%Y") )
    elif dt <= 14.:     # less than two weeks
        ax.xaxis.set_minor_locator( dates.DayLocator())
        ax.xaxis.set_minor_formatter( dates.DateFormatter("%d"))

        ax.xaxis.set_major_locator( dates.DayLocator( bymonthday=[1, 15]) )
        ax.xaxis.set_major_formatter( dates.DateFormatter("\n%b\n%Y") )
    elif dt <= 28.:     # less than four weeks
        ax.xaxis.set_minor_locator( dates.DayLocator())
        ax.xaxis.set_minor_formatter( dates.DateFormatter("%d"))

        ax.xaxis.set_major_locator( dates.MonthLocator() )
        ax.xaxis.set_major_formatter( dates.DateFormatter("\n%b\n%Y") )
    elif dt <= 4 * 30.: # less than four months
        ax.xaxis.set_minor_locator( dates.DayLocator( bymonthday=[1, 7, 14, 21] ))
        ax.xaxis.set_minor_formatter( dates.DateFormatter("%d"))

        ax.xaxis.set_major_locator( dates.MonthLocator())
        ax.xaxis.set_major_formatter( dates.DateFormatter("\n%b\n%Y") )
    else:
        ax.xaxis.set_minor_locator( dates.MonthLocator(interval=2) )
        ax.xaxis.set_minor_formatter( dates.DateFormatter("%b"))

        ax.xaxis.set_major_locator( dates.MonthLocator(bymonth=[1]) )
        ax.xaxis.set_major_formatter( dates.DateFormatter("\n%b\n%Y"))

    return ax 

def plot2d (x,y,xmin,xmax,xtitle,ytitle,varname):
#x: unixtime
    #d = [dt.datetime.utcfromtimestamp(tm.mktime(tm.localtime(i))) for i in x]
    #d = mpl.dates.date2num(d)
    # from seifertp acros-tropos/parsivel2tools


    fig   = figure(figsize=(10,3.5),dpi=80,facecolor='w',edgecolor='k')    

    ax = fig.add_subplot(111)

    delta_h=get_xtick_interval(x[0],x[-1])
    
    if delta_h[0] == 'minute':
        major    = MinuteLocator(range(0,60,delta_h[1]))
        minor    = MinuteLocator()
 
    if delta_h[0] == 'hour':
        major   = HourLocator(range(0,24,delta_h[1]))
        minor   = MinuteLocator(interval=30)

    daysFmt = DateFormatter('%d.%m.%y\n%H:%M UTC')   
    
    ax.xaxis.set_minor_locator(minor)       
    ax.xaxis.set_major_locator(major)
    ax.xaxis.set_major_formatter(daysFmt)
    
    ylocator=AutoLocator()

    ax.set_ylabel(ytitle)
    font={'family': 'sans-serif',
          'weight': 'bold',
          'size': 10}
    mpl.rc('font',**font)

    ax.yaxis.grid(True)
    ax.xaxis.grid(True)

    ax.plot_date(x,y,'-')#,tz='local')

    ax.yaxis.set_major_locator(ylocator)
    y_major  = ax.yaxis.get_majorticklocs()
    dy_minor = (y_major[-1]-y_major[0])/(len(y_major)-1)/5.
    ax.yaxis.set_minor_locator(MultipleLocator(dy_minor)) 
   
    fig.show()
    fig.savefig(varname+'.png')
    
def plotcontour (x,y,z,xmin,xmax,xtitle,ytitle,varname):

    z=z.transpose()
    z[z.lt(0)]=float('NaN')

 #   if varname == 'number_concentration':
 #       z=z[0:len(y)-1,:]        
  #      y=y[0:len(y)-1]

    y_max=y.max()
    i=z.shape[0]-1
    while (np.isnan(z.values[i,:]).all() == True or i < 0): i-=1
    i_max=i+1

    z_max=z.max().max()
#    i_max=z.columns.tolist().index((z.max().idxmax()))
    

    if z_max == 0:   z_max=0.1

    y_max = y[i_max]

    fig   = figure(figsize=(10,3.5),dpi=80,facecolor='w',edgecolor='k')    
    
    if varname == 'number_concentration':
        title='number concentration'
        cb_label='log(1/(m^3 mm))'
    if varname == 'fall_velocity':
        title='velocity distribution'
        cb_label='m/s'
        
    ax = fig.add_subplot(111,title=title)

    delta_h=get_xtick_interval(x[0],x[-1])
    
    if delta_h[0] == 'minute':
        major    = MinuteLocator(range(0,60,delta_h[1]))
        minor    = MinuteLocator()
        daysFmt  = DateFormatter('%d.%m.%y\n%H:%M UT')
 
    if delta_h[0] == 'hour':
        major   = HourLocator(range(0,24,delta_h[1]))
        minor    = MinuteLocator(interval=30)
        daysFmt = DateFormatter('%d.%m.%y\n%H:%M UT')
    
    ax.xaxis.set_minor_locator(minor)       
    ax.xaxis.set_major_locator(major)
    ax.xaxis.set_major_formatter(daysFmt)

    #ylocator=AutoLocator()

    ax.set_ylabel(ytitle)
    font={'family': 'sans-serif',
          'weight': 'bold',
          'size': 10}
    mpl.rc('font',**font)

    ax.yaxis.grid(True)
    ax.xaxis.grid(True)

    levels=[]
    n_levels=32
    z_min=0
    for i in range(n_levels):
        levels.append(z_min+i*z_max/n_levels)
    ax.set_ylim([0,y_max])
    #ax.yaxis.set_major_locator(ylocator)
    #y_major  = ax.yaxis.get_majorticklocs()
    #dy_minor = (y_major[-1]-y_major[0])/(len(y_major)-1)/5.
    #ax.yaxis.set_minor_locator(MultipleLocator(dy_minor))

    #levels=[0,0.5,1,1.5,2,2.5,3]
    if varname == 'number_concentration':
        CS=ax.contourf(x,y,z,levels)
    if varname == 'fall_velocity':
        CS=ax.contourf(x,y,z,levels)

    CS.cmap.set_under('white')
    CS.cmap.set_over('red')
    cb=fig.colorbar(CS)
    cb.ax.set_ylabel(cb_label)
    #fig.show()
    fig.savefig(varname+'.png')
    
def draw_boxplot(ax, data, edge_color, fill_color):
    bp = ax.boxplot(data, patch_artist=True)
    for element in ['boxes', 'whiskers', 'fliers', 'means', 'medians', 'caps']:
        plt.setp(bp[element], color=edge_color)
    for patch in bp['boxes']:
        patch.set(facecolor=fill_color)     
    return bp

from detecta import detect_peaks
 
def cut_DSD_event_serie(dsd,var="rain_rate",mpd=1):
    # mpd is in sample steps
    fig, ax = plot_dsd(dsd,ylims=[0,7])
    list=detect_peaks(dsd.fields[var]["data"],valley=True,mpd=mpd)
    for i in list : 
        ax.axvline(x=new_dsd.time["data"][i],color='k')
    ax.plot(dsd.time["data"],new_dsd.fields["D0"]["data"],'r-',label='D0')
    return fig
    

