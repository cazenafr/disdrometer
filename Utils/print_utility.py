#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 19 17:39:23 2022

@author: fred
"""
import datetime

#from ..DropSizeDistribution import DropSizeDistribution

def print_dsd_parameters(dsd):
    try: 
        print("location (lat,lon) = \t",dsd.location["latitude"],dsd.location["longitude"],end='\n')
    except:
        print("location not defined",end='\n')
    print("Period start time :\t",datetime.datetime.fromtimestamp(dsd.time["data"][0]).strftime('%Y-%m-%d %H:%M:%S'))
    print("Period stop time :\t",datetime.datetime.fromtimestamp(dsd.time["data"][-1]).strftime('%Y-%m-%d %H:%M:%S'))
     
    print("scattering_freq = \t",dsd.scattering_params["scattering_freq"],"Hz",end='\n')    
    print("scattering_temp = \t",dsd.scattering_params["scattering_temp"],"°C",end='\n')
    print("m_w             = \t",dsd.scattering_params["m_w"],end='\n')
    print("canting_angle   = \t",dsd.scattering_params["canting_angle"],"°",end='\n')
    print("Number of timestep of 1mn = \t",dsd.numt,end='\n')
     