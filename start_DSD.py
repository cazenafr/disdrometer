#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 14 10:34:57 2022

@author: Fred Cazenave
IGE / IRD 
"""

import sys


# Only to change spyder graphic environement 
#for p in sys.path:
#    print (p)
# to plot under qt5 
#%matplotlib qt5
# to plot inline in the spyder window 
#%matplotlib inline

from os import remove, listdir, chdir, getcwd
from os.path  import basename, dirname, splitext, exists, getsize, abspath
import matplotlib.pyplot as plt
import numpy as np
import pytmatrix
import datetime as dt
import pandas as pd

#pip install -U scikit-learn
from sklearn.metrics import r2_score

from scipy import stats

#sys.path.append("/Users/fred/opt/anaconda3/envs/top/lib/python3.9/site-packages/PyDSD-0+unknown-py3.9.egg")
import pydsd
#from  pydsd.utility.print_utility import  print_dsd_parameters
#from  pydsd.utility.func_utils import get_events, get_rain_accumulation, conv_strat_bringi_09
from  pydsd.utility.filter import filter_spectrum_with_parsivel_matrix, filter_nd_on_dropsize

#chdir(dirname(abspath(sys.argv[0])))
if sys.argv[0] == "" :
    chdir("/Users/fred/Documents/Codes/Python/Disdrometer")
else :   
    chdir(dirname(abspath(sys.argv[0])))
    
print("Current dir : ",getcwd())

#import dash
#import dash_core_components as dcc
#import dash_html_components as html

#sys.path.append("/Users/fred/opt/anaconda3/envs/top/lib/python3.9/site-packages")

try :
    exec(open(dirname(__file__)+'/Utils/func_utils.py').read())
except:
    exec(open('./Utils/func_utils.py').read())
try :
    exec(open(dirname(__file__)+'/Utils/print_utility.py').read())
except:
    exec(open('./Utils/print_utility.py').read()) 
try :
    exec(open(dirname(__file__)+'/Utils/NetCDFWriter.py').read())
except:
    exec(open('./Utils/NetCDFWriter.py').read())
try :
    exec(open(dirname(__file__)+'/Plots/plot.py').read())
except:
    exec(open('./Plots/plot.py').read())
try :
    exec(open(dirname(__file__)+'/Plots/other_plots.py').read())
except:
    exec(open('./Plots/other_plots.py').read())
try :
    exec(open(dirname(__file__)+'/Plots/dsd_TS_image_web.py').read())
except:
    exec(open('./Plots/dsd_TS_image_web.py').read())
try :
    exec(open(dirname(__file__)+'/IO/ParsivelReader_Campbell.py').read())
except:
    exec(open('./IO/ParsivelReader_Campbell.py').read())
    

'''
Nash-Sliffe Efficiency (NSE) calculation
'''
def nse(predictions, targets):
    return (1-(np.sum((predictions-targets)**2)/np.sum((targets-np.mean(targets))**2)))

'''
   This variables have to be set depending on user computer 
'''   
path_out_nc ='/Users/fred/Data/Parsivel/NCDF_OUT/'
dir_plots = '/Users/fred/Documents/Recherche/DSD/Plots/'

'''
   This variables have to be set to select Parsivel files 
''' 
Parsi_filename='/Users/fred/Data/Parsivel/CARE/CARE_vn_SPO_Parsivel2.dat'
Spect_filename='/Users/fred/Data/Parsivel/CARE/CARE_vn_SPO_Parsivel2_Spectre.dat'


# TEST SI LE FICHIER N EST PAS CORROMPU
if file_parsivel_correct(Parsi_filename) == False : sys.exit()


'''
   This variables have to be set to define the resampling interval 
''' 

resampling='1H'
resampling=None
#resampling='15min'
#resampling='5min'


'''
pydsd.read_parsivel_Campbell
  """
  Takes 2 filenames pointing to a parsivel raw file and a parsivel raw spectrum  
  and returns a drop size distribution object.
  start : define the starting date of sampling extraction - the nearest will be used
  if start is not set, extraction will start at the begining of the sampling 
  stop  : define the stoping date of the sampling extraction - the nearest will be used
  if stop is not set, extraction will end at the ending of the sampling 
  if start and stop are not set, all the sampling will be passed to the dsd object
  start and stop should be '%Y-%m-%d %H:%M:%S'
  
  if v_filter is set to True, the raw spectrum is multiplyed by de +or- 50% speed matrix
  
  resampling : resampling time of the dataset (in minutes)

  Usage:
  dsd = read_parsivel_Campbell(Parsi_filename,
                               Spect_filename,
                               start=None,
                               stop=None,
                               v_filter=None,
                               resampling=None)
  
   Takes 2 filenames pointing to a parsivel raw file and a parsivel raw spectrum  
   and returns a drop size distribution object.
   start : define the starting date of sampling extraction - the nearest will be used
   if start is not set, extraction will start at the begining of the sampling 
   stop  : define the stoping date of the sampling extraction - the nearest will be used
   if stop is not set, extraction will end at the ending of the sampling 
   if start and stop are not set, all the sampling will be passed to the dsd object
   start and stop should be '%Y-%m-%d %H:%M:%S'
   
   if v_filter is set to True, the raw spectrum is multiplyed by de +or- 50% speed matrix
   
   resampling : resampling time of the dataset
   the resampling format should be like \'5min\’ or \'H\' or \'D\' 
   see Offset aliases in df.resample Python documentation for more precision

   Returns:
   DropSizeDistrometer object

   in PyDSD-master/pydsd/io/ParsivelReader_Campbell.py
'''
dsd = read_parsivel_Campbell(Parsi_filename,
                                   Spect_filename,
                                    start="2022-06-01 00:00:00",
#                                    stop="2021-06-30 23:59:00",
#                                    v_filter=True,
                                    resampling=resampling
)
print("total rain=", np.sum(dsd.rain_rate["data"])/(np.nanmin(dsd.sampling_interval["data"])/60))
#print(dsd.fields.keys())




'''
Different ways to print the dates
'''
#print(dt.datetime.utcfromtimestamp(dsd.time["data"][0]).strftime('%Y-%m-%d %H:%M:%S'))
#print(dt.datetime.utcfromtimestamp(dsd.time["data"][-1]).strftime('%Y-%m-%d %H:%M:%S'))
#print(pd.to_datetime(dsd.time["data"][0], unit='s',utc=True))
#print(pd.to_datetime(dsd.time["data"][0], unit='s',utc=True))

'''
get_events(dsd,sep_event=10800,min_drops=None,min_acc=None)
    extract Start and stop event date -  sep_event is the time with no rain separating 2 events 
    #Data split in rain event could be done over
        - 2 events are plit by a dry period of time (sep_event by default is set to 3h) 
        - a minimum number drops (min_drops) could be set to considere a rainy period as a event (default None)
        - a minimum of rain acculalation (min_acc) could be set to considere a rainy period as a event (default None)
        - if min_drops is set min_acc is not used
    In PyDSD-master/pydsd/utility/func_utils.py  
''' 
events = get_events(dsd,sep_event=7200,min_drops=1000,min_acc=0.1)

starts=[]
for i in list(zip(*events))[0] : 
    starts.append(np.where(dsd.time["data"] == i )[0][0])

stops=[]
for i in list(zip(*events))[1] : 
    stops.append(np.where(dsd.time["data"] == i )[0][0])
    
acc=[]
for i in range(len(starts)) :
    acc.append(get_rain_accumulation (dsd, starts[i], stops[i]))

    
max_ev_start=starts[acc.index(np.nanmax(acc))]
max_ev_stop=stops[acc.index(np.nanmax(acc))]

print("Event n°",acc.index(np.nanmax(acc))," : max rain accumulation over the period : from ",
      pd.to_datetime(dsd.time["data"][max_ev_start], unit='s'),
      " to ",
      pd.to_datetime(dsd.time["data"][max_ev_stop], unit='s'),
      " : ",
      round(np.nanmax(acc),2),
      "mm")

while True:
    try:
        ev = int(input("Event to traite : "))
        if ev not in range(len(events)) : 
            print("This is not a valid event number.")
            continue
    except ValueError:
        print("This is not a valid event number.")
        continue
    else:
        break

#ev=19
ind_start= starts[ev]
#ev=198
ind_stop= stops[ev]+1

# '''
# extract_subset_dsd 
#   extracts a subset of the DSD dataset over a period between start_time and stop_time
#     New dsd object for a specfic period

#     In PyDSD-master/pydsd/utility/func_utils.py 

# '''
new_dsd=extract_subset_dsd(dsd,range(ind_start,ind_stop))

# app = dash.Dash(__name__)

# app.layout = html.Div(
#     children=[
#         html.H1(
#             children="Rain rate",
#         ),
#         html.P(
#             children="Analyze the behavior of avocado prices"
#             " and the number of avocados sold in the US"
#             " between 2015 and 2018",
#         ),
#         dcc.Graph(
#             figure={
#                 "data": [
#                     {
#                         "x": np.array(pd.to_datetime(new_dsd.time["data"], unit='s')),
#                         "y": np.array(new_dsd.rain_rate["data"]),
#                         "type": "lines",
#                     },
#                 ],
#                 "layout": {"title": "Average Price of Avocados"},
#             },
#         ),
#     ]
# )

# if __name__ == "__main__":
#     app.run_server(debug=False)

""" 
filter_spectrum_with_parsivel_matrix
    Filter a drop spectrum using fall speed matrix for Parsivels.  This requires that velocity is set on the object
    for both raw spectra and calculated terminal fall speed. If terminal fall speed is not available, this can be calculated
    using pydsd.
    Parameters
    ----------
    over_fall_speed: float, default 0.5
        Filter out drops more than this factor of terminal fall speed.
    under_fall_speed: float, default 0.5
        Filter out drops more than this factor under terminal fall speed.
    maintain_smallest: boolean, default False
        For D<1, set V<2.5 bins all to positive to make sure small drops aren't dropped in PCM matrix. 
    
    
    Returns
    -------
    filtered_spectrum_data_array: np.ndarray
        Filtered Drop Spectrum Array
    
    Example
    -------
    filter_spectrum_with_parsivel_matrix(dsd, over_fall_speed=.5, under_fall_speed=.5, replace=True)
    
    in /pydsd/utility/filter.py
"""   
filter_spectrum_with_parsivel_matrix(new_dsd,over_fall_speed=0.5,under_fall_speed=0.5,replace=True,maintain_smallest=False)

""" 
filter_nd_on_dropsize
    Filter Nd field based on a min and/or max dropsize.
    
    Parameters
    ----------
    dsd: `DropSizeDistribution` object
        DSD object to base filtering on
    drop_min: float
        Filter drops under drop_min (mm) in size.
    drop_max: float
        Filter drops larger than drop_max (mm) in size.
    replace: boolean
        Whether to overwrite the Nd in fields. If replacing, no value is returned.

    Returns
    -------
    Nd: dictionary
ion.pyt        Updated Nd dictionary. Data and a history field.
        
    in /pydsd/utility/filter.py
"""
filter_nd_on_dropsize(new_dsd, drop_min=None, drop_max=7, replace=True)


velocity=new_dsd.calculate_fall_speed(dsd.diameter["data"],inplace=True)


filter_spectrum_with_parsivel_matrix(new_dsd,over_fall_speed=0.5,under_fall_speed=0.5,replace=True,maintain_smallest=False)

""" 
calculate_dsd_from_spectrum
Calculate N(D) from the drop spectrum based on the effective sampling area.
Updates the entry for ND in fields.
Requires that drop_spectrum be present in fields, and that the dsd has spectrum_fall_velocity defined.

Parameters
----------
effective_sampling_area: function 
    Function that returns the effective sampling area as a function of diameter. Optionally
    a array with effective sampling area matched to diameter dimension can be provided as an array.
replace: boolean
    Whether to replace Nd with the newly calculated one. If true, no return value to save memory.
in PyDSD-master/pydsd/DropSizeDistribution.py
"""


'''
make_regular_TS
    New dsd object with regular time step

    In PyDSD-master/pydsd/utility/func_utils.py 

'''

new_dsd.calculate_dsd_from_spectrum(effective_sampling_area=None, replace=True)


"""
dsd.calculate_RR
Calculate instantaneous rain rate.

This calculates instantaneous rain rate based on the flux of water.
"""
new_dsd.calculate_RR()
#new_dsd=make_sampling_interval(new_dsd)

print('rain_rate = ',str(round(new_dsd.rain_rate["data"].sum()/(np.mean(np.diff(new_dsd.time["data"][0:4]))/60),2))+" mm")
print('fields[\"rain_rate\"] = ' ,str(round(new_dsd.fields["rain_rate"]["data"].sum()*np.mean(np.diff(new_dsd.time["data"][0:4]))/3600,2))+" mm")


#ax=hyetogram(new_dsd)



'''
calculate_dsd_parameterization() 
    compute  ["D0", "Dmax", "Dm", "Nt", "Nw", "N0", "W", "mu", "Lambda"]
    in PyDSD-master/pydsd/DropSizeDistribution.py
'''
new_dsd.calculate_dsd_parameterization() 
#print(new_dsd.fields.keys())

'''
set_scattering_temperature_and_frequency
    Set the scattering temperature and radar frequency
    in PyDSD-master/pydsd/DropSizeDistribution.py
'''
new_dsd.set_scattering_temperature_and_frequency(scattering_temp=25, scattering_freq=9.4e9)

'''
dsd.calculate_fall_speed
    compute the fall velocity
    in PyDSD-master/pydsd/DropSizeDistribution.py
'''
new_dsd.calculate_fall_speed(dsd.diameter["data"],inplace=True)
#velocity = 9.65 - 10.3 * np.exp(-0.6 * new_dsd.diameter["data"])
#Vel_theoric = velocity * (air_density()/air_density(temp=5,pressure=1010))**0.4

'''
dsd.calculate_radar_parameters()
    compute  [Zh, Zdr, Kdp, Ai(Attenuation), delta_co, Adr]
    Drop Shape Relationship could be selected as : 
        pb : Pruppacher and Beard DSR
        tb : Thurai and Bringi DSR
        bc : Beard and Chuang DSR
        brandes: Brandes DSR
    default is dsr_func=DSR.bc
    in PyDSD-master/pydsd/DropSizeDistribution.py
'''
new_dsd.calculate_radar_parameters() 
#print(new_dsd.fields.keys())
#ax=hyetogram(new_dsd)

'''
make_regular_TS
    New dsd object with regular time step

    In PyDSD-master/pydsd/utility/func_utils.py 

'''
new_dsd=make_regular_TS(new_dsd)
#ax=hyetogram(new_dsd)

'''
Plot scatterplot Nd raw versus Nd computed
''' 

# title='Nd raw vs Nd computed vel computed filter speed 05 diam 07'
# plt.plot(keep_Nd,new_dsd.fields["Nd"]["data"],'r.')
# x=keep_Nd
# idx = np.isfinite(x) & np.isfinite(new_dsd.fields["Nd"]["data"])
# m, b = np.polyfit(x[idx],new_dsd.fields["Nd"]["data"][idx], 1)
# r2=r2_score(x[idx],new_dsd.fields["Nd"]["data"][idx])
# plt.plot(x[idx], m*x[idx]+b, color='b'
#           ,label="reg : y="+str(round(m,3))+"x+"+str(round(b,3))+'\n'+"R2_score = "+str(round(r2,3)))
# plt.xlabel("raw Nd")
# plt.ylabel("Computed Nd")
# plt.title(title)
# plt.legend(loc='best')
# if resampling != None:
#     title=title+'_samp_'+resampling
# title=title.replace(' ','_')
# file_name = splitext(basename(Parsi_filename))[0] + '_'+title+'.png'
# plt.savefig(dir_plots+file_name)
# plt.clf()

# =============================================================================
# Plot hyetogramm
# =============================================================================
ax=hyetogram(new_dsd)
file_name = splitext(basename(Parsi_filename))[0] + '_'
file_name += "hyeto_brut_"+ \
            dt.datetime.utcfromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d_%H%M%S')+ \
            " to "+dt.datetime.utcfromtimestamp(new_dsd.time['data'][-1]).strftime('%Y%m%d_%H%M%S')
if resampling != None:
    file_name=file_name+'_samp_'+resampling
file_name=file_name.replace(' ','_')
plt.savefig(dir_plots+file_name+'.png')
plt.clf()


# =============================================================================
# Plot Velocities vs D
# =============================================================================
str_start=dt.datetime.utcfromtimestamp(new_dsd.time["data"][0]).strftime('%Y-%m-%d %H:%M:%S')
str_stop=dt.datetime.utcfromtimestamp(new_dsd.time["data"][-1]).strftime('%Y-%m-%d %H:%M:%S')

fig, ax = plot_drop_spectrum (new_dsd,
                            xlabel='Particle class size (mm)',
                            ylabel='Fall velocity classes observed by P2 (m/s)',
                            title='Raw drop size distribution\n'+ str_start+' to '+str_stop,
                            colbarlabel='log10(m$^{-3}$ mm$^{-1}$)',
                            xlim=(0,9),
                            ylim=(0,15))
ax.plot(new_dsd.diameter["data"],new_dsd.velocity["data"],'r-..',label='Velocity model')
#ax.plot(new_dsd.diameter["data"],Vel_theoric,'k-.',label='Fred understanding')
plt.legend(loc='best')
file_name = splitext(basename(Parsi_filename))[0] + '_'
file_name += "drop_spectrum_"+dt.datetime.utcfromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d_%H%M%S')+" to "+dt.datetime.utcfromtimestamp(new_dsd.time['data'][-1]).strftime('%Y%m%d_%H%M%S')
if resampling != None:
    file_name=file_name+'_samp_'+resampling
file_name=file_name.replace(' ','_')
plt.savefig(dir_plots+file_name+'.png')
plt.clf()

# =============================================================================
# Plot DSD time serie and overplot D0 and Dm
# =============================================================================
fig, ax = plot_dsd(new_dsd,ylims=[0,10])
ax.plot(new_dsd.time["data"],new_dsd.fields["D0"]["data"],'r-',label='D0')
ax.plot(new_dsd.time["data"],new_dsd.fields["Dm"]["data"],'b-',label='Dm')
plt.legend(loc='best')
fig.set_size_inches(11.69,8.27)
file_name = splitext(basename(Parsi_filename))[0] + '_'
file_name += "TS_DSD_"+dt.datetime.utcfromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d_%H%M%S')+" to "+dt.datetime.utcfromtimestamp(new_dsd.time['data'][-1]).strftime('%Y%m%d_%H%M%S')
if resampling != None:
    file_name=file_name+'_samp_'+resampling
file_name=file_name.replace(' ','_')
plt.savefig(dir_plots+file_name+'.png')
plt.clf()

# =============================================================================
# Plot DSD time serie and overplot rain_rate
# =============================================================================
fig, ax = plot_dsd(new_dsd,ylims=[0,10])
#ax.plot(new_dsd.time["data"],new_dsd.fields["D0"]["data"],'r-',label='D0')
#ax.plot(new_dsd.time["data"],new_dsd.fields["Dm"]["data"],'b-',label='Dm')
ax2=ax.twinx()
ax2.plot(new_dsd.time["data"],new_dsd.fields["rain_rate"]["data"],'r-',label='rain_rate')
plt.legend(loc='best')
fig.set_size_inches(11.69,8.27)
file_name = splitext(basename(Parsi_filename))[0] + '_'
file_name += "TS_DSD_rain_rate_"+dt.datetime.utcfromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d_%H%M%S')+" to "+dt.datetime.utcfromtimestamp(new_dsd.time['data'][-1]).strftime('%Y%m%d_%H%M%S')
if resampling != None:
    file_name=file_name+'_samp_'+resampling
file_name=file_name.replace(' ','_')
plt.savefig(dir_plots+file_name+'.png')
plt.clf()


# =============================================================================
# 4 plots on a same sheet : Hytogramm, DSD time serie, Nw and Z
# =============================================================================
generate_TS_analyse_img(new_dsd,dmax=15)


# =============================================================================
# Send a data set in convective and stratiform rain with Bring 09
# =============================================================================
conv_dsd=extract_subset_dsd(new_dsd,np.where(conv_strat_bringi_09 (new_dsd)))
strat_dsd=extract_subset_dsd(new_dsd,np.where(np.invert(conv_strat_bringi_09 (new_dsd))))

# =============================================================================
# Plot box plot of convexctive and stratiform intensities
# =============================================================================
fig, ax = plt.subplots(ncols=2)
rain_conv=conv_dsd.fields["rain_rate"]["data"][~np.isnan(conv_dsd.fields["rain_rate"]["data"])]
bp_conv=draw_boxplot(ax[0],rain_conv,'black',"red")
ax[0].set_title('Convective rain intensities ' + (resampling if resampling else ''),fontweight ="bold",fontsize = 14)
ax[0].set_ylabel("Rainfall intensities (mm/h)")
rain_strat=strat_dsd.fields["rain_rate"]["data"][~np.isnan(strat_dsd.fields["rain_rate"]["data"])]
rain_strat = rain_strat[np.where(rain_strat !=0)]
bp_strat=draw_boxplot(ax[1],rain_strat,'black',"green")
ax[1].set_title('Stratiform rain intensities '+(resampling if resampling else ''),fontweight ="bold",fontsize = 14)
ax[1].set_ylabel("Rainfall intensities (mm/h)")
fig.show()
print("rain_conv Intensities")
print("Max",str('%.2f'%rain_conv.max()))
print("Sup ends",str('%.2f'%(np.quantile(rain_conv,0.75) + 1.5 * np.quantile(rain_conv,0.75)-np.quantile(rain_conv,0.25))))
print("Q75",str('%.2f'%np.quantile(rain_conv,0.75)))
print("median",str('%.2f'%np.median(rain_conv)))
print("Q25",str('%.2f'%np.quantile(rain_conv,0.25)))

print("rain_strat Intensities")
print("Max",str('%.2f'%rain_strat.max()))
print("Sup Ends",str('%.2f'%(np.quantile(rain_strat,0.75) + 1.5 * np.quantile(rain_strat,0.75)-np.quantile(rain_strat,0.25))))
print("Q75",str('%.2f'%np.quantile(rain_strat,0.75)))
print("median",str('%.2f'%np.median(rain_strat)))
print("Q25",str('%.2f'%np.quantile(rain_strat,0.25)))

# =============================================================================
# Plop pies of convective and stratiform rain intensities and accumulations
# =============================================================================
fig, ax = plt.subplots(ncols=2)
data=[rain_conv.sum()/12,rain_strat.sum()/12]
ax[0].pie(data, labels = None,
          colors = ['red', 'green'],
 #         normalize = True,
          autopct = lambda data: str(round(data, 2)) + '%',
          pctdistance = 0.7, labeldistance = 1.4)
ax[0].legend(labels = ['Conv', 'Strat'])
ax[0].set_title('Rain accumulation '+str('%.2f'%(rain_conv.sum()/12+rain_strat.sum()/12)) + 'mn',
                    fontweight ="bold",fontsize = 14)
data=[len(rain_conv)*12,len(rain_strat)*12]
ax[1].pie(data, labels = None,
          colors = ['red', 'green'],
#          normalize = True,
          autopct = lambda data: str(round(data, 2)) + '%',
          pctdistance = 0.7, labeldistance = 1.4)
ax[1].legend(labels = ['Conv', 'Strat'])
ax[1].set_title('Rain duration '+ str('%.0f'%(len(rain_conv)*5+len(rain_strat)*5)) + 'mn',
                    fontweight ="bold",fontsize = 14)


# =============================================================================
# Plot scatter plot and hytogramm of Conv Start rain
# =============================================================================
fig=plot_conv_strat_BR09(new_dsd)
file_name = splitext(basename(Parsi_filename))[0] + '_'
file_name += "Conv_Strat_BR09_"+dt.datetime.utcfromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d_%H%M%S')+" to "+dt.datetime.utcfromtimestamp(new_dsd.time['data'][-1]).strftime('%Y%m%d_%H%M%S')
if resampling != None:
    file_name=file_name+'_samp_'+resampling
file_name=file_name.replace(' ','_')
fig.set_size_inches(11.69,8.27)
fig.savefig(dir_plots+file_name+'.png')
fig.clf()

# =============================================================================
# Plot the DSD shape for Conv and strat rain
# =============================================================================
fig = plot_conv_strat_DSD(new_dsd)
file_name = splitext(basename(Parsi_filename))[0] + '_'
file_name += "Conv_Strat_DSD_"+dt.datetime.utcfromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d_%H%M%S')+" to "+dt.datetime.utcfromtimestamp(new_dsd.time['data'][-1]).strftime('%Y%m%d_%H%M%S')
if resampling != None:
    file_name=file_name+'_samp_'+resampling
file_name=file_name.replace(' ','_')
#fig.set_size_inches(11.69,8.27)
fig.savefig(dir_plots+file_name+'.png')
fig.clf() 

# =============================================================================
# Compute and plot Z-R regression conv and strat
# =============================================================================


from scipy import stats

###ALL
X=np.array(new_dsd.fields["rain_rate"]["data"][np.where(new_dsd.fields["Zh"]["data"]> 10)])
Y=np.array(10**(new_dsd.fields["Zh"]["data"][np.where(new_dsd.fields["Zh"]["data"]> 10)]/10))
index=X.argsort(axis=None).reshape(X.shape)
X=X[index]
Y=Y[index]
fig, ax1 = plt.subplots(1, 1, figsize=(8,6))
ax1.scatter(X,Y,c='tab:blue')
slope, intercept, r_value, p_value, std_err = stats.linregress(np.log(X), np.log(Y))
fitLine = intercept + np.multiply(np.log(X), slope)
ax1.plot(X,np.exp(fitLine), c='r')
ax1.set_yscale('log')
ax1.set_xscale('log')
ax1.set_ylabel('10**(Z(dBZ)/10)')
ax1.set_xlabel('Rain_Rate(mm)')
ax1.set_title("Reflectivity versus Rain_rate : all samples")
plt.text(10**2, 10**4, "a="+f'{np.exp(intercept):.2f}'+"\nb="+f'{slope:.2f}'+"\nR2="+f'{r_value:.2f}')
plt.show()
file_name = splitext(basename(Parsi_filename))[0] + '_'
file_name += "All_Z_vs_R"+dt.datetime.utcfromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d_%H%M%S')+" to "+dt.datetime.utcfromtimestamp(new_dsd.time['data'][-1]).strftime('%Y%m%d_%H%M%S')
if resampling != None:
    file_name=file_name+'_samp_'+resampling
file_name=file_name.replace(' ','_')
#fig.set_size_inches(11.69,8.27)
fig.savefig(dir_plots+file_name+'.png')
fig.clf() 
print("ALL a=",f'{np.exp(intercept):.2f}',", b=",f'{slope:.2f}'," R2=",f'{r_value:.2f}')


###CONV
id_ocean=np.where(conv_strat_Thompson_15(conv_dsd))
id_cont=np.where(conv_strat_Thompson_15(conv_dsd) == False)
X=np.array(conv_dsd.fields["rain_rate"]["data"][np.where(conv_dsd.fields["Zh"]["data"]> 10)])
Y=np.array(10**(conv_dsd.fields["Zh"]["data"][np.where(conv_dsd.fields["Zh"]["data"]> 10)]/10))
index=X.argsort(axis=None).reshape(X.shape)
X=X[index]
Y=Y[index]
fig, ax1 = plt.subplots(1, 1, figsize=(8,6))
ax1.scatter(X,Y,c='tab:red',label="Continental")
#ax1.scatter(X[id_ocean],Y[id_ocean],c='tab:orange',label="Oceanic")
slope, intercept, r_value, p_value, std_err = stats.linregress(np.log(X), np.log(Y))
fitLine = intercept + np.multiply(np.log(X), slope)
ax1.plot(X,np.exp(fitLine), c='r',label="linear reg.")
ax1.set_yscale('log')
ax1.set_xscale('log')
ax1.set_ylabel('10**(Z(dBZ)/10)')
ax1.set_xlabel('Rain_Rate(mm)')
ax1.set_title("Reflectivity versus Rain_rate : conv. samples")
plt.text(10**2, 10**4, "a="+f'{np.exp(intercept):.2f}'+"\nb="+f'{slope:.2f}'+"\nR2="+f'{r_value:.2f}')
#ax1.legend(loc="best")
plt.show()
file_name = splitext(basename(Parsi_filename))[0] + '_'
file_name += "Conv_Z_vs_R"+dt.datetime.utcfromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d_%H%M%S')+" to "+dt.datetime.utcfromtimestamp(new_dsd.time['data'][-1]).strftime('%Y%m%d_%H%M%S')
if resampling != None:
    file_name=file_name+'_samp_'+resampling
file_name=file_name.replace(' ','_')
#fig.set_size_inches(11.69,8.27)
fig.savefig(dir_plots+file_name+'.png')
fig.clf() 
print("CONV a=",f'{np.exp(intercept):.2f}',", b=",f'{slope:.2f}'," R2=",f'{r_value:.2f}')


####STRAT
X=np.array(strat_dsd.fields["rain_rate"]["data"][np.where(strat_dsd.fields["Zh"]["data"]> 10)])
Y=np.array(10**(strat_dsd.fields["Zh"]["data"][np.where(strat_dsd.fields["Zh"]["data"]> 10)]/10))
index=X.argsort(axis=None).reshape(X.shape)
X=X[index]
Y=Y[index]
fig, ax1 = plt.subplots(1, 1, figsize=(8,6))
ax1.scatter(X,Y)
slope, intercept, r_value, p_value, std_err = stats.linregress(np.log(X), np.log(Y))
fitLine = intercept + np.multiply(np.log(X), slope)
ax1.plot(X,np.exp(fitLine), c='r')
ax1.set_yscale('log')
ax1.set_xscale('log')
ax1.set_ylabel('10**(Z(dBZ)/10)')
ax1.set_xlabel('Rain_Rate(mm)')
ax1.set_title("Reflectivity versus Rain_rate : strat. samples")
plt.text(5, 10**2, "a="+f'{np.exp(intercept):.2f}'+"\nb="+f'{slope:.2f}'+"\nR2="+f'{r_value:.2f}')
plt.show()
file_name = splitext(basename(Parsi_filename))[0] + '_'
file_name += "Strat_Z_vs_R"+dt.datetime.utcfromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d_%H%M%S')+" to "+dt.datetime.utcfromtimestamp(new_dsd.time['data'][-1]).strftime('%Y%m%d_%H%M%S')
if resampling != None:
    file_name=file_name+'_samp_'+resampling
file_name=file_name.replace(' ','_')
#fig.set_size_inches(11.69,8.27)
fig.savefig(dir_plots+file_name+'.png')
fig.clf() 
print("START a=",f'{np.exp(intercept):.2f}',", b=",f'{slope:.2f}'," R2=",f'{r_value:.2f}')

mpd=4
fig=cut_DSD_event_serie(new_dsd,"rain_rate",mpd=mpd)
fig.set_size_inches(11.69,8.27)
file_name = splitext(basename(Parsi_filename))[0] + '_'
file_name += "DSD_cuts"+dt.datetime.utcfromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d_%H%M%S')+" to "+dt.datetime.utcfromtimestamp(new_dsd.time['data'][-1]).strftime('%Y%m%d_%H%M%S')
if resampling != None:
    file_name=file_name+'_samp_'+resampling
file_name=file_name.replace(' ','_')
fig.savefig(dir_plots+file_name+'.png')
fig.clf() 

dir_spectrums=dir_plots+dt.datetime.fromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d')+"_Spectrums"
try:
    os.makedirs(dir_spectrums)
except OSError:
    print('Already exists')
else:
    print('Directory created')

from detecta import detect_peaks
list=detect_peaks(new_dsd.fields["rain_rate"]["data"],valley=True,mpd=mpd)
list=np.insert(list, 0, 0)
list[-1]=len(new_dsd.fields["Dm"]["data"])-1
Part=[]
for i in range(len(list)-1) : 
    print(range(list[i],list[i+1]))
    Part.append(extract_subset_dsd(new_dsd,range(list[i],list[i+1]+1)))
    X=np.array(Part[i].fields["rain_rate"]["data"][np.where(Part[i].fields["Zh"]["data"]> 10)])
    Y=np.array(10**(Part[i].fields["Zh"]["data"][np.where(Part[i].fields["Zh"]["data"]> 10)]/10))
    index=X.argsort(axis=None).reshape(X.shape)
    X=X[index]
    Y=Y[index]
    fig, ax1 = plt.subplots(1, 1, figsize=(8,6))
    ax1.scatter(X,Y)
    slope, intercept, r_value, p_value, std_err = stats.linregress(np.log(X), np.log(Y))
    fitLine = intercept + np.multiply(np.log(X), slope)
    ax1.plot(X,np.exp(fitLine), c='r')
    ax1.set_yscale('log')
    ax1.set_xscale('log')
    ax1.set_ylabel('10**(Z(dBZ)/10)',fontsize=12)
    ax1.set_xlabel('Rain_Rate(mm)',fontsize=12)
    ax1.set_title("Reflectivity versus Rain_rate",fontsize=12)
    plt.text(0.1,0.6, "a="+f'{np.exp(intercept):.2f}'+"\nb="+f'{slope:.2f}'+"\nR2="+f'{r_value:.2f}',transform=ax1.transAxes,fontsize=30)
    plt.show()
    file_name = splitext(basename(Parsi_filename))[0] + '_'
    file_name += "Part_"+str(i)+"_"+dt.datetime.utcfromtimestamp(new_dsd.time['data'][0]).strftime('%Y%m%d_%H%M%S')+" to "+dt.datetime.utcfromtimestamp(new_dsd.time['data'][-1]).strftime('%Y%m%d_%H%M%S')
    if resampling != None:
        file_name=file_name+'_samp_'+resampling
    file_name=file_name.replace(' ','_')
    #fig.set_size_inches(11.69,8.27)
    fig.savefig(dir_plots+file_name+'.png')
    fig.clf() 

    dates=[]
    D0=[]
    Dm=[]
    DM=[]
    DC=[]
    R=[]
    Mu=[]
    fig=plot_spectrums_DSD(Part[i])
    file_name = "Part_"+str(i)+"_"+dt.datetime.fromtimestamp(Part[i].time['data'][0]).strftime('%Y%m%d')+\
                    "_spectrums_norm"
    file_name=file_name+'.png'
    plt.savefig(dir_spectrums+"/"+file_name)
    plt.clf()
    fig=plot_spectrums_DSD(Part[i],norm=False)
    file_name = "Part_"+str(i)+"_"+dt.datetime.fromtimestamp(Part[i].time['data'][0]).strftime('%Y%m%d')+ \
                "_spectrums"
    file_name=file_name+'.png'
    plt.savefig(dir_spectrums+"/"+file_name)
    plt.clf()
    fig=plot_DSD_params(Part[i])
    file_name = "Part_"+str(i)+"_"+dt.datetime.fromtimestamp(Part[i].time['data'][0]).strftime('%Y%m%d')+ \
        "_Param_DSD"
    file_name=file_name+'.png'
    plt.savefig(dir_spectrums+"/"+file_name)
    plt.clf()



#generate_TS_analyse_img(new_dsd,dmax=15,add_title="Convective")

# =============================================================================
# Save data to NCDF file
# =============================================================================

# new_dsd.fields["rain_rate"] = common.var_to_dict(
#     "Rain rate", new_dsd.fields["rain_rate"]["data"], "mm/h", "Rain rate"
# )
# file_out = splitext(basename(Parsi_filename))[0] + '_'
# file_out += dt.datetime.utcfromtimestamp(new_dsd.time["data"][0]).strftime('%Y%m%d_%H%M%S') + '_'
# file_out += dt.datetime.utcfromtimestamp(new_dsd.time["data"][-1]).strftime('%Y%m%d_%H%M%S') 
# if resampling != None:
#     file_out=file_out+'_samp_'+resampling
# file_out += '.nc'
    
# if exists(path_out_nc+file_out) :
#     remove(path_out_nc+file_out) 
# write_netcdf(new_dsd,path_out_nc+file_out)

# '''
# to read back DSD store in a NetCDF file
# '''
# from netCDF4 import Dataset
# ncDSD = Dataset(path_out_nc+file_out)
# print(ncDSD.variables.keys()) # get all variable names
# for d in ncDSD.dimensions.items():
#   print(d) # get all dimensions
# #to access to variable 'rain_rate'
# ncDSD.variables['rain_rate'] # or
# ncDSD['rain_rate']
# #to access to the dataset 'rain_rate'
# ncDSD.variables['rain_rate'][:].data #or 
# ncDSD['rain_rate'][:].data









# Hetogram function has been modified to use DSD struct
# the code below won't wrok anymore
# print("Hyetogram plot")
# ax=hyetogram(ncDSD)
# file_name = splitext(basename(Parsi_filename))[0] + '_'
# file_name += "hyeto_"+dt.datetime.utcfromtimestamp(ncDSD['time'][0].data.item()).strftime('%Y%m%d_%H%M%S')+" to "+dt.datetime.utcfromtimestamp(ncDSD['time'][-1].data.item()).strftime('%Y%m%d_%H%M%S')
# if resampling != None:
#     file_name=file_name+'_samp_'+resampling
# file_name=file_name.replace(' ','_')
# plt.savefig(dir_plots+file_name+'.png')
# plt.clf()

# '''
# Calcul Nash sur filtre V(d)
# '''
# events = get_events(dsd,sep_event=10800,min_drops=1000,min_acc=0.1)

# starts=[]
# for i in list(zip(*events))[0] : 
#     starts.append(np.where(dsd.time["data"] == i )[0][0])

# stops=[]
# for i in list(zip(*events))[1] : 
#     stops.append(np.where(dsd.time["data"] == i )[0][0])
    
# acc=[]
# for i in range(len(starts)) :
#     acc.append(get_rain_accumulation (dsd, starts[i], stops[i]))

# for ev in range(len(events)) : 
#     ind_start=starts[ev]
#     ind_stop=stops[ev]
#     '''
#     extract_subset_dsd 
#         New dsd object for a specfic period
    
#         In PyDSD-master/pydsd/utility/func_utils.py 
    
#     '''


# to comput the NASH on rain_rate filtererd depending on fall speed tolerance 
# new_dsd =extract_subset_dsd(dsd,dsd.time["data"][ind_start],dsd.time["data"][ind_stop])
    
# Ori_rain=new_dsd.fields["rain_rate"]["data"][:]
# nash=[]
# sum_RR=[]
# for i in range(1,500) : 
#     new2=copy.deepcopy(new_dsd)
#     filter_spectrum_with_parsivel_matrix(new2,over_fall_speed=i/1000,under_fall_speed=i/1000,replace=True,maintain_smallest=True)
#     new2.calculate_dsd_from_spectrum(effective_sampling_area=None, replace=True)
#     new2.calculate_RR()
#     nash.append(nse(new2.fields["rain_rate"]["data"][:],Ori_rain))
#     sum_RR.append(new2.fields["rain_rate"]["data"].sum()*new_dsd.sampling_interval['data'][0]/3600)
    
# print("Event ", ev,np.where(nash == np.max(nash)))
# #### fin du de la boucle For 
        
# plt.plot(np.arange(1,500)/1000,nash)
# plt.xlabel("Theorical fall speed ± x")
# plt.ylabel("Nash-Sliffe Efficiency")
# ax2 = plt.twinx()
# ax2.plot(np.arange(1,500)/1000,sum_RR,"r-")
# ax2.set_ylabel(r"Rain accumulation (mm)",color='r')
# plt.title("Nash and Rain acc depending on theorical speed tolerance")
# np.where(nash == np.max(nash))   


